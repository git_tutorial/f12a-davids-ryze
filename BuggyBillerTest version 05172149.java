package ryze.billing;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import ryze.analytics.BuggyAnalysis;

public class BuggyBillerTest {

	@Test
	public void buggybiller_allZeroAnalysis_0(){
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(0,0,0,0,0.0);
		assertEquals(0.0, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}

	@Test
	public void buggybiller_lowerRate_LowerRateMultiplyTotalWordCountExpected(){ // change test name
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(10,55,1,10,5.5);
		//int expected = 165;
		assertEquals(165, (bb.bill( Arrays.asList(lysis)).get(1))*3, 0.01);
	}
	
	@Test
	public void buggybiller_higherRate_HigherRateMultiplyTotalWordCountExpected() {// change test name
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(10,58,1,10,5.8);
		assertEquals(406, (bb.bill( Arrays.asList(lysis)).get(1))*7, 0.01);
	}
}
