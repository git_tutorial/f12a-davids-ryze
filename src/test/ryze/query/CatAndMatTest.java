package ryze.query;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class CatAndMatTest {

	@Test
	public void SmallFileTextQuery_AnyKeyword_ThreeLinesReturned() {
		SmallFileTextQuery query = new SmallFileTextQuery();  // instantiate the query
		List<String> result = query.query("cat", "dog");    // start the query using these keywords
		assertEquals(3, result.size());  // JUnit assert that 3 lines are returned
	}
	
	@Test
	public void SmallFileTextQuery_WholeWordKeyword_OneLineReturned(){
		SmallFileTextQuery query = new SmallFileTextQuery();
		query.setFileName("data/cat_and_catalogue.txt");  // this is how to use another file 
		List<String> result = query.query("cat"); 
		assertEquals(1, result.size());  // JUnit assert that only 1 line is returned
	}

	@Test
	public void SmallFileTextQuery_KeywordCaseSensitivity_CaseInsensitive(){
		SmallFileTextQuery query = new SmallFileTextQuery();
		query.setFileName("data/cat_and_mat.txt");  
		List<String> lowercaseResult = query.query("cat"); 
		
		List<String> uppercaseResult = query.query("CAT");
		
		List<String> firstUppercaseResult = query.query("Cat");
		
		assertEquals(lowercaseResult, uppercaseResult);  // JUnit assert that the results are equal 
		assertEquals(uppercaseResult, firstUppercaseResult); // JUnit assert that the results are equal
	}
	
	@Test
	public void SmallFileTextQuery_KeywordCaseSensitivity_Case_Insensitive_MyDesignedTestCase(){
		
		fail(); //TODO remove this line before running this test
		
		//TODO for you to modifiy
				
		SmallFileTextQuery query = new SmallFileTextQuery();
		query.setFileName("data/<your_own_file>"); //TODO use your own file here  
		List<String> lowercaseResult = query.query("replace", "with", "your", "own", "keywords"); 
		
		List<String> uppercaseResult = query.query("replace", "with", "uppercase or lowercase", "variants", "of previous", "keywords");
		
		List<String> firstUppercaseResult = query.query("replace", "with", "mixed case", "variants", "of previous", "keywords");
		
		assertEquals(lowercaseResult, uppercaseResult);  // JUnit assert that the results are equal 
		assertEquals(uppercaseResult, firstUppercaseResult); // JUnit assert that the results are equal

		
	}
}
