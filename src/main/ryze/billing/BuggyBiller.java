package ryze.billing;

import java.util.Arrays;
import java.util.List;

import ryze.analytics.BuggyAnalysis;

public class BuggyBiller implements Biller<BuggyAnalysis,Double> {

	public double lowRate, highRate;
	
	public BuggyBiller(){}
	
	public BuggyBiller(double loRate, double hiRate){
		lowRate = loRate;
		highRate = hiRate;
	}

	@Override
	public List<Double> bill(List<? extends BuggyAnalysis> analysis) {
		BuggyAnalysis lysis = analysis.get(0);
		double medianLineLength = (lysis.getMaxLineLength() - lysis.getMinLineLength())/2;
		double applRate = lowRate;
		if(medianLineLength > lysis.getAvgLineLength()){
			applRate = highRate;
		}
		else
		{
			applRate = lowRate;
		}
		return Arrays.asList(new Double[]{applRate*lysis.getTotalLines()});
	}
	//
	

}
