package ryze.analytics;

import java.util.LinkedList;
import java.util.List;

public class BuggyAnalyzer implements Analyzer<String, BuggyAnalysis>{

	@Override
	public List<BuggyAnalysis> analyze(List<String> lines) {
		List<BuggyAnalysis> res = new LinkedList<>();
		
		BuggyAnalysis lysis = new BuggyAnalysis();
		res.add(lysis);
		
		lysis.setTotalLines(calcTotalLines(lines));
		lysis.setTotalWords(calcTotalWords(lines));
		lysis.setMinLineLength(calcMinLineLength(lines));
		lysis.setMaxLineLength(calcMaxLineLength(lines));
		lysis.setAvgLineLength(calcAvgLineLength(lines));
		return res;
	}

	//calculating the average line length//
	private double calcAvgLineLength(List<String> lines) {
		return (double)calcTotalWords(lines)/calcTotalLines(lines);
	}

	//calculating the max line length//
	private int calcMaxLineLength(List<String> lines) {
		int maxLineLen = 0;
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String str1 = line.replaceAll("[^0-9a-zA-Z]" , " ");
			String str2 = str1.trim();
			String[] words = str2.split("\\s{0,}");
			int lineLen = 0;
			for(int j=0; j<words.length; j++){
				lineLen++;
			}
			if(lineLen > maxLineLen )
			{
				maxLineLen = lineLen;
			}
		}
		return maxLineLen;
	}

	//calculating the min line length//
	private int calcMinLineLength(List<String> lines) {
		int minLineLen = 0;
		String line01 = lines.get(0);
		String str1 = line01.replaceAll("[^0-9a-zA-Z]" , " ");
		String str2 = str1.trim();
		String [] words01 = str2.split("\\s{0,}");
		for (int c = 0; c < words01.length; c++)
		{
			minLineLen++;
		}
		
		for(int i=1; i<lines.size(); i++){
			String line = lines.get(i);
			String str1 = line.replaceAll("[^0-9a-zA-Z]" , " ");
			String str2 = str1.trim();
			String[] words = str2.split("\\s{0,}");
			int lineLen = 0;
			for(int j=0; j<words.length; j++){
				lineLen++;
			}
			if(lineLen < minLineLen ) minLineLen = lineLen;

				
		}
		return minLineLen;
	}

	//calculating the total words counts//
	private int calcTotalWords(List<String> lines) {
		int wordcount = 0;
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			for(int j=0; j<words.length; j++){
				wordcount = wordcount + 1;
			}
		}
		return wordcount;
	}

	//calculating the total lines//
	private int calcTotalLines(List<String> lines) {
		int count = 0;
		for(int i=0; i<lines.size();i++){
			count = count + 1;
		}
		return count;
	}

}
