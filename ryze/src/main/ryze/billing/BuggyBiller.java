package ryze.billing;

import java.util.Arrays;
import java.util.List;

import ryze.analytics.BuggyAnalysis;

public class BuggyBiller implements Biller<BuggyAnalysis,Double> {

	public double lowRate, highRate;
	
	public BuggyBiller() {
		
	}
	
	public BuggyBiller(double loRate, double hiRate){
		lowRate = loRate;
		highRate = hiRate;
	}

	@Override
	public List<Double> bill(List<? extends BuggyAnalysis> analysis) {
		BuggyAnalysis lysis = analysis.get(0);
		double medianLineLength = (double)(lysis.getMaxLineLength() + lysis.getMinLineLength())/2;
		//System.out.println(medianLineLength);
		//System.out.println(lysis.getMaxLineLength());
		//System.out.println(lysis.getMinLineLength());
		//System.out.println(lysis.getAvgLineLength());
		//System.out.println("meow");
		double applRate = lowRate;
		if(medianLineLength < lysis.getAvgLineLength()){
			applRate = highRate;
		}
		else
		{
			applRate = lowRate;
		}
		//System.out.println(applRate);
		//System.out.println("meow");
		return Arrays.asList(new Double[]{applRate*lysis.getTotalWords()});
	}
	//
	

}
