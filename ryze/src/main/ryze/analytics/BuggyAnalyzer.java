package ryze.analytics;

import java.util.LinkedList;
import java.util.List;

public class BuggyAnalyzer implements Analyzer<String, BuggyAnalysis>{

	@Override
	public List<BuggyAnalysis> analyze(List<String> lines) {
		List<BuggyAnalysis> res = new LinkedList<>();
		
		BuggyAnalysis lysis = new BuggyAnalysis();
		res.add(lysis);
		
		lysis.setTotalLines(calcTotalLines(lines));
		lysis.setTotalWords(calcTotalWords(lines));
		lysis.setMinLineLength(calcMinLineLength(lines));
		lysis.setMaxLineLength(calcMaxLineLength(lines));
		//System.out.println("Done~" + calcAvgLineLength(lines));
		lysis.setAvgLineLength(calcAvgLineLength(lines));
		lysis.setMinusMaxMin(calcMinusMaxMin(lines));
		lysis.setMaxMinEqual(isMaxMinEqual(lines));
		return res;
	}

	//calculating the average line length//
	private double calcAvgLineLength(List<String> lines) {
		//System.out.println(calcTotalWords(lines));
		//System.out.println(calcTotalLines(lines));//if 
		if (calcTotalLines(lines) == 0)
		{
			return 0;
		}
		return (double)calcTotalWords(lines)/calcTotalLines(lines);
	}

	//calculating the max line length//
	private int calcMaxLineLength(List<String> lines) {
		int maxLineLen = 0;
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String str1 = line.replaceAll("[^a-zA-Z]" , " ");
			String str2 = str1.trim();
			String[] words = str2.split("[\\s]+");
			int lineLen = 0;
			for(int j=0; j<words.length; j++){
				lineLen++;
			}
			if(lineLen > maxLineLen ) maxLineLen = lineLen;	
		}
		return maxLineLen;
	}
	
	//calculating the min line length//
	private int calcMinLineLength(List<String> lines) {
		int minLineLen = 0;
		if (lines.size()== 0)
		{
			return 0;
		}
		String line01 = lines.get(0);
		String str1 = line01.replaceAll("[^a-zA-Z]" , " ");
		String str2 = str1.trim();
		String [] words01 = str2.split("[\\s]+");
		for (int c = 0; c < words01.length; c++)
		{
			minLineLen++;
		}
		
		for(int i=1; i<lines.size(); i++){
			String line = lines.get(i);
			String str3 = line.replaceAll("[^a-zA-Z]" , " ");
			String str4 = str3.trim();
			String[] words = str4.split("[\\s]+");
			int lineLen = 0;
			for(int j=0; j<words.length; j++){
				lineLen++;
			}
			if(lineLen < minLineLen ) minLineLen = lineLen;

				
		}
		return minLineLen;
	}

	//calculating the total words counts//
	private int calcTotalWords(List<String> lines) {
		int wordcount = 0;
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String str1 = line.replaceAll("[^a-zA-Z]" , " ");
			String str2 = str1.trim();
			String[] words = str2.split("[\\s]+");
//			for (int j = 0;j<words.length;j++)
//			{
//				System.out.println(words[j]);
//			}
			for(int j=0; j<words.length; j++){
				wordcount = wordcount + 1;
			}
		}
		return wordcount;
	}

	//calculating the total lines//
	private int calcTotalLines(List<String> lines) {
		int count = 0;
		for(int i=0; i<lines.size();i++){
			count = count + 1;
		}
		return count;
	}
	
	
	//enhancement #1 max - min//
	private int calcMinusMaxMin(List<String> lines){
		return calcMaxLineLength(lines) - calcMinLineLength(lines);
	}
	
//	private int calcAttentionRate(List<String> lines) {
//		int rate = 
//	}
	
	private boolean isMaxMinEqual(List<String> lines){
		if (calcMaxLineLength(lines) == calcMinLineLength(lines))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
