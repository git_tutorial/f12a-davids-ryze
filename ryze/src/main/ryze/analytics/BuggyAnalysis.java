package ryze.analytics;

public class BuggyAnalysis {

	private int totalLines, totalWords, maxLineLength, minLineLength;
	
	private int minusMaxMin;
	
	private boolean maxMinEqual;
	
	private double avgLineLength;

	public BuggyAnalysis(){}
	
	public BuggyAnalysis(int totalLines, int totalWords, int maxLineLength, int minLineLength, double avgLineLength) {
		super();
		this.totalLines = totalLines;
		this.totalWords = totalWords;
		this.maxLineLength = maxLineLength;
		this.minLineLength = minLineLength;
		this.avgLineLength = avgLineLength;
	}
	
	public int getMinusMaxMin() {
		return minusMaxMin;
	}
	
	public boolean getMaxMinEqual() {
		return maxMinEqual;
	}
	public void setMaxMinEqual (boolean maxMinEqual) {
		this.maxMinEqual = maxMinEqual;
	}
	
	public int getTotalLines() {
		return totalLines;
	}
	public void setMinusMaxMin (int minusMaxMin){
		this.minusMaxMin = minusMaxMin;
	}
	public void setTotalLines(int totalLines) {
		this.totalLines = totalLines;
	}
	public int getTotalWords() {
		return totalWords;
	}
	public void setTotalWords(int totalWords) {
		this.totalWords = totalWords;
	}
	public int getMaxLineLength() {
		return maxLineLength;
	}
	public void setMaxLineLength(int maxLineLength) {
		this.maxLineLength = maxLineLength;
	}
	public int getMinLineLength() {
		return minLineLength;
	}
	public void setMinLineLength(int minLineLength) {
		this.minLineLength = minLineLength;
	}
	public double getAvgLineLength() {
		return avgLineLength;
	}
	public void setAvgLineLength(double avgLineLength) {
		this.avgLineLength = avgLineLength;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(avgLineLength);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + maxLineLength;
		result = prime * result + minLineLength;
		result = prime * result + totalLines;
		result = prime * result + totalWords;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuggyAnalysis other = (BuggyAnalysis) obj;
		System.out.println(Double.doubleToLongBits(avgLineLength));
		System.out.println(Double.doubleToLongBits(other.avgLineLength));
		if (Double.doubleToLongBits(avgLineLength) != Double.doubleToLongBits(other.avgLineLength))
			return false;
		if (maxLineLength != other.maxLineLength)
			return false;
		if (minLineLength != other.minLineLength)
			return false;
		if (totalLines != other.totalLines)
			return false;
		if (totalWords != other.totalWords)
			return false;
		if (minusMaxMin != other.minusMaxMin)
			return false;
		return true;
	}
}