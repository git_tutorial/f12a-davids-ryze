package ryze.ui;

import java.util.List;

import ryze.analytics.Analyzer;
import ryze.analytics.CountAnalyzer;
import ryze.billing.Biller;
import ryze.billing.CountBiller;
import ryze.query.TwitterTextQuery;

public class RyzeCommandLineUI {

	public static void main(String[] args) throws Exception{
		TwitterTextQuery tq = new TwitterTextQuery(200);
		
		Analyzer<String, Integer> ca = new CountAnalyzer();
		
		Biller<Number, Double> bi = new CountBiller(0.01);
		System.out.printf("Obtaining tweets...%n");
		
		List<String> tweets = tq.query(args);
		
		
		List<Integer> counts = ca.analyze(tweets);
		
		List<Double> bill = bi.bill(counts);
		
		System.out.printf("Charge of AUS$%.2f based on %d tweets containing the keywords [%s]%n",
				bill.get(0),
				counts.get(0),
				String.join(", ", args)
				);
		System.out.printf("Charge of USD$%.2f based on %d tweets containing the keywords [%s]%n",
				bill.get(0) * 0.72,
				counts.get(0),
				String.join(", ", args)
				);
		System.out.printf("Charge of EURO€%.2f based on %d tweets containing the keywords [%s]%n",
				bill.get(0) * 0.65,
				counts.get(0),
				String.join(", ", args)
				);
		
		//System.out.println(args);
		//System.out.println(bill.get(0));
		
		int count = 0;
		for(String t: tweets){
			if (count >= 10)
				break;
			System.out.println(t);
			System.out.println();
			count++;
		}
		
		
		
		
	}
}
