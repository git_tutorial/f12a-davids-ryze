
package ryze.ui;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import org.rythmengine.Rythm;

import fi.iki.elonen.SimpleWebServer;
import fi.iki.elonen.util.ServerRunner;
import ryze.web.ExecuteWorkflowHandler;
import ryze.web.Handler;
import ryze.web.HelloHandler;
import ryze.web.WorkflowsHandler;

public class RyzeWebUI extends SimpleWebServer {

    /**
     * logger to log to.
     */
    private static final Logger LOG = Logger.getLogger(RyzeWebUI.class.getName());

	public static final String port = Optional.ofNullable(System.getenv("PORT")).orElse("8443");
	public static final String wwwroot = Optional.ofNullable(System.getenv("WWW_ROOT")).orElse("www");
//	public static final String cors = Optional.ofNullable(System.getenv("CORS")).orElse("*");

    public static void main(String[] args) {
    	String templatePath = new File("templates").getAbsolutePath();
    	Map<String, Object> conf = new HashMap<>();
    	conf.put("home.template", templatePath);
    	conf.put("rythm.engine.mode", "prod");
    	conf.put("rythm.log.enabled", false);
    	Rythm.init(conf);
    	
    	
        ServerRunner.run(RyzeWebUI.class);
    }

    public RyzeWebUI() {
        super(null, Integer.parseInt(port), new File(wwwroot), true );
        
        handlers.put(getKey(Method.GET, "/hello"), new HelloHandler());
        handlers.put(getKey(Method.GET, "/"), new WorkflowsHandler());
        handlers.put(getKey(Method.POST, "/"), new WorkflowsHandler());
        handlers.put(getKey(Method.GET, "/workflows"), new WorkflowsHandler());
        handlers.put(getKey(Method.POST, "/workflows"), new WorkflowsHandler());
        handlers.put(getKey(Method.POST, "/execute-workflow"), new ExecuteWorkflowHandler());

    }

    Map<String, Handler> handlers = new HashMap<>();
    
    private String getKey(Method method, String uri){
    	
    	String key = method.toString() + " " + uri;
    	return key;
    }

    private Response dispatch(Method method, String uri, IHTTPSession session){
    	
    	String key = getKey(method, uri);
    	Handler h = handlers.get(key);
    	if(h != null){
    		return h.handle(method, uri, session);
    	}else{
    		return super.serve(session);
    	}
    }
    
    @Override
    public Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String uri = session.getUri();
        RyzeWebUI.LOG.info(method + " '" + uri + "' ");

        return dispatch(method, uri, session);
    }
}