package ryze.query;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import java.util.Scanner;
//import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
public class BuggyFileTextQueryTest {

	
	@Test
	public void buggyFileTextQuery_10xUniversity_10Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university.txt");
		List<String> res = query.query("university");
		assertEquals(10, res.size());
	}
	

	@Test
	public void buggyFileTextQuery_10xUniversity1xComment_9Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_1x_comment.txt");
		List<String> res = query.query("university");
		assertEquals(9, res.size());
	}
	@Test
	public void buggyFileTextQuery_10xUniversity1stComment_9Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_1st_comment.txt");
		List<String> res = query.query("university");
		//version 05062058//
		assertEquals (9, res.size());
		//fail(); 
	}

	@Test
	public void buggyFileTextQuery_10xUniversityLastComment_9Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_last_comment.txt");
		List<String> res = query.query("university");
		assertEquals (9, res.size());
		
	}

	@Test
	public void buggyFileTextQuery_10xUniversity1stAndLastComment_8Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_1st_and_last_comment.txt");
		List<String> res = query.query("university");
		assertEquals (8, res.size());
	
	}
	
	@Test
	public void buggyFileTextQuery_emptyfile_0Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/empty-0lines.txt");
		List<String> res = query.query("university");
		assertEquals (0, res.size());
		//fail();
	}

	@Test
	public void buggyFileTextQuery_whitespacebefore4comment_6expected() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();//fail();
		query.setFileName("data/whitespacebefore4comment.txt");
		List<String> res = query.query("university");
		assertEquals (6 ,res.size());
	}
	
	@Test
	public void buggyFileTextQuery_alllinewithcomment_0expected() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/alllineswithcommented.txt");
		List<String> res = query.query("university");
		assertEquals (0 ,res.size());
	}
	
    @Test
    public void buggyFileTextQuery_firstfifthlastlinescommentedwithdifferentspaces_7expected() {
    	BuggyFileTextQuery query = new BuggyFileTextQuery();
    	query.setFileName("data/firstfifthlastlinescommentedwithdifferentspaces.txt");
		List<String> res = query.query("university");
		assertEquals (7 ,res.size());
    }
	
    @Test
    public void buggyFileTextQuery_nocommentlinesbutalsonokeyword_0expected() {
    	BuggyFileTextQuery query = new BuggyFileTextQuery();
    	query.setFileName("data/nocommentlinesbutalsonokeyword.txt");
		List<String> res = query.query("university");
		assertEquals (0 ,res.size());
    }
	
    //this is the version on the day of 05100953 and it is all correct//
    
    //a new test case for the tutorial
    @Test
    public void bugyFileTextQuery_tutorial_correctresultexpected() {
    	BuggyFileTextQuery query = new BuggyFileTextQuery();
    	query.setFileName("data/tutorial.txt");//this is test file you can change the content in it
    	File name = new File ("data/tutorial.txt");
    	
    	//ArrayList <String> input = new ArrayList <String> ();
    	try
    	{
    		Scanner reader = new Scanner (name);
    		int count = 0;
    	    
    	    
    	    String [] inn = new String [10000];
    	    int i = 0;
    	    while (reader.hasNextLine())
    	    {
    		    count = count+1;
    		    inn [i] = reader.nextLine();
    		    i = i+1;
    		    //System.out.println(inn [i]);
    	    }
    	
    	
    	    reader.close ();
	    	List<String> res = query.query("University");//you can change the keyword
	    	assertEquals (4 ,res.size());//you can change the expected output
	    	System.out.println("There we have "+res.size() + " line which have keyword.");
	    	System.out.println(res.size() + " out of " + count + " lines have the keyword");
	    	//System.out.println("the precentage is " + (double)(res.size()/count));
	    	double answer = 0.0;
	    	if (count != 0)
	    	{
	    		answer = (double)res.size() / (double)count;
	    	}
	    	else
	    	{
	    		answer = 0.0;
	    	}
	    	System.out.println("the precentage is " + answer);
    	}
    	catch (FileNotFoundException e)
    	{
    		e.printStackTrace ();
    	}
    	
    }
	
}
