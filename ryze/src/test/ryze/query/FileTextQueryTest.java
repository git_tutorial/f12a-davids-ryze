package ryze.query;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class FileTextQueryTest {

	@Test
	public void fileTextQuery_DefaultFile_WarNPeace(){
		FileTextQuery query = new FileTextQuery();
		assertEquals(query.getFileName(), "data/war_and_peace.txt");
	}
	
	@Test
	public void fileTextQuery_WarNPeaceNumLines_64999() {
		FileTextQuery query = new FileTextQuery();
		List<String> res = query.query(new String[0]);
		assertEquals(64999, res.size());
	}
	

	@Test
	public void fileTextQuery_LinesWithEngland_15(){
		
		FileTextQuery query = new FileTextQuery();
		List<String> res = query.query(new String[]{"eNGlaND"}); //test case-insensitive
		assertEquals(15, res.size());
	}

	@Test
	public void fileTextQuery_LinesWithPrussia_30(){
		
		FileTextQuery query = new FileTextQuery();
		List<String> res = query.query(new String[]{"PruSsIa"}); //test case-insensitive
		assertEquals(30, res.size());
	}
	
	
	@Test
	public void fileTextQuery_LinesWithAustria_31(){
		
		FileTextQuery query = new FileTextQuery();
		List<String> res = query.query(new String[]{"aUsTRIa"}); //test case-insensitive
		assertEquals(31, res.size());
	}
	@Test

	public void fileTextQuery_LinesWithRussia_173(){
		
		FileTextQuery query = new FileTextQuery();
		List<String> res = query.query(new String[]{"ruSsIa"}); //test case-insensitive
		assertEquals(173, res.size());
	}

}
