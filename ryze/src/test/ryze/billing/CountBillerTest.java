package ryze.billing;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ryze.analytics.Analyzer;
import ryze.analytics.CountAnalyzer;
import ryze.query.TwitterTextQuery;

public class CountBillerTest {

	List<String> tweets; 
	List<Integer> counts;

	@Before
	public void setUp() throws Exception {
		TwitterTextQuery tq = new TwitterTextQuery(200);
		
		Analyzer<String, Integer> ca = new CountAnalyzer();
		
		tweets = tq.query("hello", "world");
		counts = ca.analyze(tweets);
		
	}

	@Test
	public void countBiller_atOneCentPerTweet_OneCentTimesNumberOfTweets() {
		double rate = 0.01;
		CountBiller ba = new CountBiller ();
		ba.setRate (rate);
		int count = counts.get(0);
		Biller<Number,Double> bi = new CountBiller(ba.getRate ());
		
		List<Double> bill = bi.bill(counts);
		
		assertEquals(bill, Arrays.asList(rate*count));
	}
	
	@Test
	public void countBiller_atTwoCentPerTweet_TwoCentTimesNumberOfTweets(){
		//fail();
		double rate = 0.02;
		CountBiller ba = new CountBiller ();
		ba.setRate (rate);
		int count = counts.get(0);
		Biller<Number,Double> bi = new CountBiller(ba.getRate ());
		List<Double> bill = bi.bill(counts);
		
		assertEquals (bill, Arrays.asList(rate*count));
		//System.out.println(bill);
		System.out.println(count);
	}

}
