package ryze.billing;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import ryze.analytics.BuggyAnalysis;

public class BuggyBillerTest {

	@Test
	public void buggybiller_allZeroAnalysis_0(){
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(0,0,0,0,0.0);
		assertEquals(0.0, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}

	@Test
	public void buggybiller_lowerRate_LowerRateMultiplyTotalWordCountExpected(){ 
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(10,55,10,1,5.5);
	    
		//issue need to be resolved
		//there is a problem
		assertEquals(165, bb.bill( Arrays.asList(lysis)).get(0), 0.01);
	}
	
	@Test
	public void buggybiller_higherRate_HigherRateMultiplyTotalWordCountExpected() {
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(10,58,10,1,5.8);
		
		//issue need to be resolved
		//there is a problem
		assertEquals(406, bb.bill( Arrays.asList(lysis)).get(0), 0.01);
	} 
	
	@Test
	public void buggybiller_lowrate_LowerRateMultiplyTotalWordCountExpected() {
		BuggyBiller bb = new BuggyBiller(2,22);
		BuggyAnalysis lysis = new BuggyAnalysis(10,52,1,10,5.2);
		
		//issue need to be resolved
		//there is a problem
		assertEquals(104, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}
	
	@Test
	public void buggybiller_lowrate_correctExpected() {
		BuggyBiller bb = new BuggyBiller(2,22);
		BuggyAnalysis lysis = new BuggyAnalysis(10,100,10,10,10);
		
		//issue need to be resolved
		//there is a problem
		assertEquals(200, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}
}
