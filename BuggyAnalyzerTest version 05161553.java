package ryze.analytics;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class BuggyAnalyzerTest {

	
	@Test
	public void buggyanalyzer_nolines_all0inbuggyanalysis(){
		List<String> noLines = new  LinkedList<>(); //here is an example on how to create input corresponding to no input lines
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(noLines).get(0);
		assertEquals(new BuggyAnalysis(0,0,0,0,0.0), lysis);
	}
	
	@Test
	public void buggyanalyzer_threeLines_totalLinesIs3(){
		List<String> threeLines = Arrays.asList(new String[]{"hello", "beautiful", "world"}); // an example of short input
		BuggyAnalyzer ba = new BuggyAnalyzer();
		
		BuggyAnalysis lysis = ba.analyze(threeLines).get(0);
		assertEquals(new BuggyAnalysis(3,3,1,1,1.0), lysis);
	}

	@Test
	public void buggyanalyzer_multLines_fivecorrectnumberExpected(){ //TODO change test name
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"was",
				"the",
				"best",
				"of",
				"times, it was the worst",
				"of times"
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		//TODO you guys to do this
		//fail();
		assertEquals(new BuggyAnalysis(7,12,5,1,1.714), lysis);
	}

	@Test
	public void buggyanalyzer_wordDelimitation_totalwordcountis18(){ //TODO change test name
		List<String> wordDelimitation = Arrays.asList(new String[]{"hello#i ,am --the-peolpe who$live%on the.earth",
				"it ,was,very&&happy7we can ^^stay*here"
				//TODO construct lines to test word delimitation
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordDelimitation).get(0); 
		//TODO you guys to do this
		//fail();
		assertEquals(18, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_wordCount_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> wordCount = Arrays.asList(new String[]{
				"i,am**&^%*&living in a big&&&&city##called sydney&&&&&"//TODO construct lines to test word delimitation
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordCount).get(0); 
		//TODO you guys to do this
		//fail();
		assertEquals(9, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_totalWordCount_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalWordCount = Arrays.asList(new String[]{
				"ni^^^^^zhi%%^DAOao&__________mamama"//TODO construct lines to test word delimitation
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalWordCount).get(0); 
		//TODO you guys to do this
		assertEquals(4, lysis.getTotalWords());
		//fail();
	}

	@Test
	public void buggyanalyzer_avgLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> avgLineLen = Arrays.asList(new String[]{
				"this%%%is",
				"my",
				"testing()()()()iii",
				"aaa",
				"bbb",
				":ccc"
				//TODO construct lines to test word delimitation
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(avgLineLen).get(0); 
		//TODO you guys to do this
		assertEquals(
			1.33, // TODO change to your_expected_value> 
			lysis.getAvgLineLength(),
			0.01 // we accept an error 0.01 and less
		);
		
		//fail();
	}
	@Test
	public void buggyanalyzer_minLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> minLineLen= Arrays.asList(new String[]{
				"this%%%is",
				"my",
				"testing()()()()iii",
				"aaa",
				"bbb",
				":ccc"//TODO construct lines to test word delimitation
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(minLineLen).get(0); 
		assertEquals(1, lysis.getMinLineLength());
		//TODO you guys to do this
		//fail();
	}
	@Test
	public void buggyanalyzer_maxLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> maxLineLen= Arrays.asList(new String[]{
				"this%%%is",
				"my",
				"testing()()()()iii{{{{{{{{{insinskxn**&(()&^*(^*testing ha ha hahaha!!!",
				"aaa",
				"bbb",
				":ccc"//TODO construct lines to test word delimitation
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(maxLineLen).get(0); 
		//TODO you guys to do this
		//fail();
		assertEquals (7, lysis.getMaxLineLength());
	}
	
//	public void buggyanalyzer_MinusMaxMin_resultExpected(){
//		List<String> minusMaxMin= Arrays.asList(new String[]{
//				"this is",
//				"a",
//				"big big big &&& idea hahaha"
//		});
//		BuggyAnalyzer ba = new BuggyAnalyzer();
//		BuggyAnalysis lysis = ba.analyze(minusMaxMin).get(0);
//		assertEquals (4, lysis.getMinusMaxMin());
//	}
}
