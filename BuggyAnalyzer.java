package ryze.analytics;

import java.util.LinkedList;
import java.util.List;

public class BuggyAnalyzer implements Analyzer<String, BuggyAnalysis>{

	@Override
	public List<BuggyAnalysis> analyze(List<String> lines) {
		List<BuggyAnalysis> res = new LinkedList<>();
		
		BuggyAnalysis lysis = new BuggyAnalysis();
		res.add(lysis);
		
		lysis.setTotalLines(calcTotalLines(lines));
		lysis.setTotalWords(calcTotalWords(lines));
		lysis.setMinLineLength(calcMinLineLength(lines));
		lysis.setMaxLineLength(calcMaxLineLength(lines));
		lysis.setAvgLineLength(calcAvgLineLength(lines));
		return res;
	}

	private double calcAvgLineLength(List<String> lines) {
		return calcTotalWords(lines)/calcTotalLines(lines);
	}

	private int calcMaxLineLength(List<String> lines) {
		int maxLineLen = 0;
		for(int i=1; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			int lineLen = 0;
			for(int j=1; j<words.length; j++){
				lineLen++;
			}
			if(lineLen < maxLineLen ) maxLineLen = lineLen;
		}
		return maxLineLen;
	}

	private int calcMinLineLength(List<String> lines) {
		int minLineLen = 0;
		for(int i=1; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			int lineLen = 0;
			for(int j=1; j<words.length; j++){
				lineLen++;
			}
			if(lineLen > minLineLen ) minLineLen = lineLen;
		}
		return minLineLen;
	}

	private int calcTotalWords(List<String> lines) {
		int wordcount = 0;
		for(int i=1; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			for(int j=1; j<words.length; j++){
				wordcount = wordcount + 1;
			}
		}
		return wordcount;
	}

	private int calcTotalLines(List<String> lines) {
		int count = 0;
		for(int i=1; i<lines.size();i++){
			count = count + 1;
		}
		return count;
	}

}
